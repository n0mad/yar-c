#include "rasterizer.h"

int create_ppm_image(int width, int height) {
	char *out;
	/* render */
	printf("P3\n%d %d\n255\n", width, height);

	for(int i = height-1; i >=0; --i) {
		/* progress bar */
		fprintf(stderr, "\rscanlines remaining: %d\n", i);
		for(int j = 0; j < width; ++j) {
			vec3_t pixel_color = vec3_from_float((float)(j) / (width -1),
					(float)(i) / (height -1), 0.25f);

			write_color(&out, pixel_color);
			printf("%s\n", out);
		}
	}

	//end
	fprintf(stderr, "[DONE]\n");

	free(out);
	return 0;
}

int create_ppm_image_cam(camera_t *cam) {
	char *out;
	/* render */
	printf("P3\n%d %d\n255\n", cam->img_w, cam->img_h);

	for(int i = cam->img_h-1; i >=0; --i) {
		/* progress bar */
		//fprintf(stderr, "\rscanlines remaining: %d\n", i);
		for(int j = 0; j < cam->img_w; ++j) {
			float u = (float)(j) / (cam->img_w -1);
			float v = (float)(i) / (cam->img_h -1);

			//we need a ray here, and a custom operator for the direction apparently
			//aand it's not working
			vec3_t _s1 = vec3_add(cam->lower_left_corner, vec3_float_multiply(cam->hor, u));
			vec3_t _s2 = vec3_add(_s1, vec3_float_multiply(cam->ver, v));
			vec3_t _dir = vec3_subtract(_s2, cam->orig);

			//fprintf(stderr, "ppm_cam: u is %.2f, v is %.2f\n", u, v);
			//fprintf(stderr, "ppm_cam: _dir.y is %.2f\n", _dir.y);
			ray_t ray = create_ray(cam->orig, _dir);
			vec3_t pixel_color = ray_color(&ray);

			write_color(&out, pixel_color);
			printf("%s\n", out);
		}
	}

	//end
	//fprintf(stderr, "[DONE]\n");

	free(out);
	return 0;
}


#include "vec3.h"

vec3_t vec3_zero(void) {
	vec3_t res = {0.0f, 0.0f, 0.0f};
	return res;
}

vec3_t vec3_one(void) {
	vec3_t res = {1.0f, 1.0f, 1.0f};
	return res;
}

vec3_t vec3_x(void) {
	vec3_t res = {1.0f, 0.0f, 0.0f};
	return res;
}

vec3_t vec3_y(void) {
	vec3_t res = {0.0f, 1.0f, 0.0f};
	return res;
}

vec3_t vec3_z(void) {
	vec3_t res = {0.0f, 0.0f, 1.0f};
	return res;
}

vec3_t vec3_from_float(float x, float y, float z) {
	vec3_t res;

	res.x = x;
	res.y = y;
	res.z = z;

	return res;
}

vec3_t vec3_add(vec3_t vec_a, vec3_t vec_b) {
	vec3_t res = {(vec_a.x + vec_b.x), (vec_a.y + vec_b.y), (vec_a.z + vec_b.z)};
	return res;
}

vec3_t vec3_subtract(vec3_t vec_a, vec3_t vec_b) {
	vec3_t res = {(vec_a.x - vec_b.x), (vec_a.y - vec_b.y), (vec_a.z - vec_b.z)};
	return res;
}

vec3_t vec3_float_multiply(vec3_t vec_a, float scalar) {
	vec3_t res = {vec_a.x * scalar, vec_a.y * scalar, vec_a.z * scalar};
	return res;
}

vec3_t vec3_float_div(vec3_t vec_a, float value) {
	vec3_t res = {(vec_a.x / value), (vec_a.y / value), (vec_a.z / value)};
	return res;
}

vec3_t vec3_multiply(vec3_t vec_a, vec3_t vec_b) {
	vec3_t res = {(vec_a.x * vec_b.x), (vec_a.y * vec_b.y), (vec_a.z * vec_b.z)};
	return res;
}

float vec3_length(vec3_t vec_a) {
	float res = sqrtf((vec_a.x * vec_a.x) + (vec_a.y * vec_a.y) + (vec_a.z * vec_a.z));
	return res;
}

float vec3_norm(vec3_t vec_a) {
	float res = (vec_a.x * vec_a.x) + (vec_a.y * vec_a.y) + (vec_a.z * vec_a.z);
	return res;
}

vec3_t vec3_unit(vec3_t vec_a) {
	vec3_t res = vec3_zero();
	float vec_length = vec3_length(vec_a);
	res.x = res.x / vec_length;
	res.y = res.y / vec_length;
	res.z = res.z / vec_length;
	//float vec_length = vec3_length(vec_a);

	/*if(vec_length != 0.0f) {
		res.x = res.x / vec_length;
		res.y = res.y / vec_length;
		res.z = res.z / vec_length;
	}*/

	return res;
}


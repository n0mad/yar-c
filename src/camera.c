#include "camera.h"

camera_t *init_camera() {
	camera_t *_cam = calloc(1, sizeof(camera_t));

	_cam->aspect_ratio = 16.0f / 9.0f;
	_cam->img_w = 400;
	_cam->img_h = (int)(_cam->img_w / _cam->aspect_ratio);

	_cam->v_h = 2.0f;
	_cam->v_w = _cam->aspect_ratio * _cam->v_h;
	_cam->focal_len = 1.0f;

	_cam->orig = vec3_zero();
	_cam->hor = vec3_from_float(_cam->v_w, 0.0f, 0.0f);
	_cam->ver = vec3_from_float(0.0f, _cam->v_h, 0.0f);

	fprintf(stderr, "cam img_h is %d\n", _cam->img_h);
	fprintf(stderr, "cam aspect is %.2f\n", _cam->aspect_ratio);

	//vec_a from subtract is orig - hor/2
	//vec_b from subtract is ver/2 - vec3(0,0,focal_len)
	//we need to implement vec3_float_subtract
	_cam->lower_left_corner = vec3_subtract(
			vec3_subtract(_cam->orig, vec3_float_div(_cam->hor, 2.0f)),
			vec3_subtract(vec3_float_div(_cam->ver, 2.0f),
				vec3_from_float(0.0f, 0.0f, _cam->focal_len)));

	fprintf(stderr, "cam llc is %.2f, %.2f, %.2f\n", _cam->lower_left_corner.x, _cam->lower_left_corner.y, _cam->lower_left_corner.z);

	return _cam;
}


#include "color.h"

void write_color(char **out, vec3_t pixel_color) {
	// here we can try to dynamically allocate memory to our out var. Maybe useless.
	int _bufsize = snprintf(NULL, 0, "%d %d %d\n",
			(int)(255.999 * pixel_color.x),
			(int)(255.999 * pixel_color.y),
			(int)(255.999 * pixel_color.z));

	if(_bufsize < 0) {
		//print some error and exit
		fprintf(stderr, "err: failed to determine buffer size\n");
		exit(1);
	}

	//allocate the our buffer. +1 for nul terminator
	*out = malloc(_bufsize +1);

	//now actually write to the out buffer
	sprintf(*out, "%d %d %d\n",
			(int)(255.999 * pixel_color.x),
			(int)(255.999 * pixel_color.y),
			(int)(255.999 * pixel_color.z));
}

vec3_t ray_color(ray_t *ray) {
	float unit_y = ray->dir.y / vec3_norm(ray->dir);
	double t = 0.5f * (unit_y + 1.0f);

	//apparently our vec3_unit func does not operate the same way as the guide
	//vec3_t unit_dir = vec3_unit(ray->dir);
	//double _t = 0.5f * (unit_dir.y + 1.0f);


	//draw a sky-like background
	vec3_t _w = vec3_from_float(1.0f, 1.0f, 1.0f);
	vec3_t _b = vec3_from_float(0.5f, 0.7f, 1.0f);
	vec3_t _r = vec3_add(vec3_float_multiply(_w, (1.0f-t)), vec3_float_multiply(_b, t));

	return _r;
}


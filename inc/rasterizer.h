#ifndef RASTERIZER_H
#define RASTERIZER_H

#include "camera.h"
#include "color.h"

int create_ppm_image(int width, int height);

int create_ppm_image_cam(camera_t *cam);

#endif //RASTERIZER_H


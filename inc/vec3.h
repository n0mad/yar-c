#ifndef VEC3_H
#define VEC3_H

#include <math.h>
#include <stdio.h>

typedef struct {
	float x, y, z;
} vec3_t;

vec3_t vec3_zero(void);

vec3_t vec3_one(void);

vec3_t vec3_x(void);

vec3_t vec3_y(void);

vec3_t vec3_z(void);

vec3_t vec3_from_float(float x, float y, float z);

vec3_t vec3_add(vec3_t vec_a, vec3_t vec_b);

vec3_t vec3_subtract(vec3_t vec_a, vec3_t vec_b);

vec3_t vec3_float_multiply(vec3_t vec_a, float scalar);

vec3_t vec3_float_div(vec3_t vec_a, float value);

vec3_t vec3_multiply(vec3_t vec_a, vec3_t vec_b);

float vec3_length(vec3_t vec_a);

float vec3_norm(vec3_t vec_a);

vec3_t vec3_unit(vec3_t vec_a);

#endif //VEC3_H


#ifndef RAY_H
#define RAY_H

#include "vec3.h"

// a ray works from an origin towards a direction. vec3 please!
typedef struct {
	vec3_t orig, dir;
} ray_t;

ray_t create_ray(vec3_t orig, vec3_t dir);

vec3_t ray_at(ray_t ray, double t);

#endif // RAY_H


#ifndef COLOR_H
#define COLOR_H

#include <stdlib.h>
#include <string.h>
#include "ray.h"

void write_color(char **out, vec3_t pixel_color);

vec3_t ray_color(ray_t *ray);

#endif // COLOR_H


#ifndef CAMERA_H
#define CAMERA_H

#include <stdlib.h>
#include "vec3.h"

typedef struct {
	float v_h, v_w; //v stands for viewport
	float focal_len;
	vec3_t orig, hor, ver; //need to guess hor and ver
	vec3_t lower_left_corner; //need to guess this one too
	float aspect_ratio;
	int img_w, img_h; //img ones could be out of this struct
} camera_t;

// allocates memory for a camera struct, TODO add parameters so user can modify values
camera_t *init_camera();

#endif // CAMERA_H


CC = clang

SRCS != ls src/*.c
OBJS = $(SRCS:.c=.o)
DIST = dist

INC = -Iinc -I/usr/local/include
CFLAGS = -c -g -Wall $(INC) -fsanitize=address
LDFLAGS = -L/usr/local/lib -lm -fsanitize=address

EXEC_NAME = test

## build exec ##
all: output install

output: $(OBJS)
	@echo Building $(EXEC_NAME) ...
	$(CC) $(OBJS) $(LDFLAGS) -o $(EXEC_NAME)

$(OBJS): $(SRCS)
	@echo Compiling srcs ...
	$(CC) $(CFLAGS) -c $< -o $@

## remove things ##
.PHONY: clean install

install:
	@echo Moving $(EXEC_NAME) into $(DIST)...
	@if [ -f $(EXEC_NAME) ]; then\
		mv $(EXEC_NAME) $(DIST)/$(EXEC_NAME);\
	fi

clean:
	rm src/*.o
	if [ -f "$(DIST)/$(EXEC_NAME)" ]; then\
		rm "$(DIST)/$(EXEC_NAME)";\
	fi
	clear

